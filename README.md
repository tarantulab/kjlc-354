# KJLC 354 Series Ionization Vacuum gauge driver

## Intro

A Platform agnostic, `no_std`, serial communication driver for the KJLC 354 Series Ionization Vacuum gauge, based on the embedded-hal traits.

- Implements the RS485 communication protocol specification for the KJLC 354.
- Includes the feature flag "extra" for commands that might not be present on older devices.

## Status

The driver is technically complete, but some further testing is required for some commands as to verify nothing behaves in a weird way.

### Why not use type states for the query functionality?

Type states were considered at some point but it sacrifices some comfiness in certain use-cases. This just means the user must make sure the sequence `schedule_message`-`send_message`-`poll` is correctly followed.

## License

Licensed under Apache-2.0.

### Contributions

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in this work by you shall be under the terms and conditions of
the Apache-2.0 license, without any additional terms or conditions.

## Additional info

 Kurt J. Lesker provides the datasheet, troubleshooting guide and manual, you can find them [here](https://www.lesker.com/newweb/gauges/ionization_kjlc_354.cfm).

![Image of the KJLC 354](https://www.lesker.com/newweb/images/product_photos/Photo-IG-KJLC354_full.jpg)
