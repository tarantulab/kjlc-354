/// Used to represent diferent error states during operation.
/// Errors sent by the transducer are wrapped with the Kjlc354Error Value.
#[derive(Debug)]
pub enum DriverError<ReadError, WriteError> {
    FormattingError,
    BadMessage,
    SerialRead(ReadError),
    SerialWrite(WriteError),
}

impl<ReadError, WriteError> From<core::fmt::Error>
    for DriverError<ReadError, WriteError>
{
    fn from(_err: core::fmt::Error) -> Self {
        DriverError::<ReadError, WriteError>::FormattingError
    }
}

/// Errors found when parsing a response
#[derive(Debug, PartialEq, Eq)]
pub enum ResponseError {
    InstrumentError(Kjlc354Error),
    FailedToParseResponse,
    IllegalState,
}

/// Types of errors that the instrument communicates trough error codes in response
/// messages. `UnknownError` has no corresponding code in the manual, it is there
/// to catch possible illegal states produced by malfunctions in communication
/// (like a failure in the serial port/connection)
#[derive(Debug, PartialEq, Eq)]
pub enum Kjlc354Error {
    SyntaxError,
    CommunicationsError,
    UnknownError,
}

