use core::{fmt::{self, Display, Formatter}};
use heapless::Vec;

use crate::{scinumber::SciNumber, MAX_STR_LEN};

/// Valid baudrates for serial communications
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum BaudRate {
    X300,
    X600,
    X1200,
    X2400,
    X4800,
    X9600,
    X14400,
    X19200,
    X28800,
    X38400,
    X57600,
}
impl BaudRate {
    pub fn into_integer(&self) -> u32 {
        match &self {
            BaudRate::X300 => 300,
            BaudRate::X600 => 600,
            BaudRate::X1200 => 1200,
            BaudRate::X2400 => 2400,
            BaudRate::X4800 =>  4800,
            BaudRate::X9600 =>  9600,
            BaudRate::X14400 => 14400,
            BaudRate::X19200 => 19200,
            BaudRate::X28800 => 28800,
            BaudRate::X38400 => 38400,
            BaudRate::X57600 => 57600,
        }
    }
}
impl Display for BaudRate {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.into_integer())
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum AddressOffset {
    NoOffset, //00
    Offset10, //10
    Offset20, //20
    Offset30, //30
}
impl Display for AddressOffset{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self{
            AddressOffset::NoOffset => write!(f,"00"),
            AddressOffset::Offset10 => write!(f,"10"),
            AddressOffset::Offset20 => write!(f,"20"),
            AddressOffset::Offset30 => write!(f,"30"),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum EmissionCurrent {
    /// For 5E-2 to 5E-6 Torr pressure range
    I100uA, //0
    /// For 5E-4 to 5E-9 Torr pressure range
    I40mA, //1
}
impl Display for EmissionCurrent{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self{
            EmissionCurrent::I100uA => write!(f, "0"),
            EmissionCurrent::I40mA => write!(f, "1"),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Filament {
    One, //F1
    Two, //F2
}
impl Display for Filament{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self{
            Filament::One => write!(f, "1", ),
            Filament::Two => write!(f, "2", ),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum PressurePoint {
    TurnsOnBelow, //+
    TurnsOnAbove, //-
}
impl Display for PressurePoint{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self{
            PressurePoint::TurnsOnBelow => write!(f, "+"),
            PressurePoint::TurnsOnAbove => write!(f, "-"),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Parity {
    None, //N
    Odd,  //O
    Even, //E
}
impl Display for Parity{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self{
            Parity::None => write!(f, "N", ),
            Parity::Odd => write!(f, "O", ),
            Parity::Even => write!(f, "E", ),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum PressureUnit {
    Torricelli, //T
    MiliBar,     //M
    Pascal,      //P
}
impl Display for PressureUnit{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self{
            PressureUnit::Torricelli => write!(f,"T"),
            PressureUnit::MiliBar => write!(f, "M"),
            PressureUnit::Pascal => write!(f, "P"),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Status{
    Ok,
    PowerCycle, 
    Overpressure,
    EmissionCurrentFailure,
    EmissionCurrentFailureAfterPowerCycle,
    IonCurrentFailure
}

/// Values that can be queried from the instrument
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Value {
    /// Read the current displayed pressure
    IonGaugePressure, //RD
    /// Read if the Ion Gauge is On
    IonGaugeIsOn, //IGS
    /// Read the pressure points for RLY1
    TripPointRelay1(PressurePoint), //RL
    /// Find out if the module is currently degassing
    DegasStatusIsOn, //DGS
    /// Find out emission current level
    EmissionCurrentStatus, //SES
    /// Read Ion Gauge Status. If the device was shut down
    /// by an error, this provides the cause for the shut down
    IonGaugeModuleStatus, //RS
    /// Read the part number and revision number(version) of
    /// the firmware
    SoftwareVersion, //VER

    //EXTRA
    #[cfg(feature = "extra")]
    PressureUnit, //RU
    #[cfg(feature = "extra")]
    IonGaugeIonCurrent, //RDIGC
    #[cfg(feature = "extra")]
    IonGaugeEmissionCurrent, //RDIGE
    #[cfg(feature = "extra")]
    IonGaugeFilamentVoltage, //RDIGV
    #[cfg(feature = "extra")]
    IonGaugeFilamentCurrent, //RDIGI
}

/// Settings in the instrument that can be modified by the user
#[derive(Clone, Debug, PartialEq)]
pub enum Setting {
    /// Configure Adress Offset (upper nible)
    /// Won't take effect until next Reset.
    AddressOffset(AddressOffset), //SA
    /// Turn On/Off the Ion Gauge
    /// Note: Use only below 1.00E-3 Torr. Always turn off the IG filament
    /// manually before pressure is to rise above 1.00E-3 Torr
    TurnIonGaugeOn(bool), //IG
    /// Start a Degas Cycle.
    /// Should only done below 5E-5 Torr
    TurnDegasOn(bool), //DG
    EmissionCurrent(EmissionCurrent), //SE
    Filament(Filament),               //SF
    SetOverPressurePoint(SciNumber),  //SO
    TripPointRelay1(PressurePoint, SciNumber), //SL
    /// Set all configurations to factory defaults.
    /// Won't take effect until next Reset.
    SetFactoryDefaults, //FAC
    /// Set the BaudRate for serial communication.
    /// Won't take effect until next Reset.
    BaudRate(BaudRate), //SB
    /// Set the Parity for serial communication.
    /// Won't take effect until next Reset.
    Parity(Parity), //SP
    /// Will toggle the UNL Lock. If UNL lock is
    /// ON, the UnlockCommsConfig command must be
    /// sent first before any baudrate or parity
    /// commnad. If UNL Lock is off, sending the
    /// UnlockCommsConfig Command will generate
    /// a Syntax Error
    ToggleUNLFunction, //TLU
    /// If the UNL Lock is activated,
    /// This command must be sent before
    /// configuring baudrate or parity.
    UnlockCommsConfig, //UNL
    /// Reset the device as if power was cycled
    /// (Required to complete some commands)
    Reset, //RST
    #[cfg(feature = "extra")]
    /// Set Pressure Unit for display and RD response
    PressureUnit(PressureUnit), //SU
}

/// Types of messages/transactions
#[derive(Clone, Debug, PartialEq)]
pub enum Message {
    Value(Value),
    Setting(Setting),
    None,
}
impl Display for Message {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Message::Value(value) => write!(f, "{}", value),
            Message::Setting(setting) => write!(f, "{}", setting),
            Message::None => write!(f, "404"),
        }
    }
}
impl From<Setting> for Message {
    fn from(setting: Setting) -> Self {
        Message::Setting(setting)
    }
}
impl From<Value> for Message {
    fn from(value: Value) -> Self {
        Message::Value(value)
    }
}

/// Possible data types for data inside a response message
#[derive(Clone, PartialEq, Debug)]
pub enum Response {
    Boolean(bool),
    Text(Vec<u8, MAX_STR_LEN>),
    SciNumber(SciNumber),
    Status(Status),
    EmissionCurrent(EmissionCurrent),
    PressureUnit(PressureUnit)
    
}
impl Display for Response {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Response::Boolean(val) => {
                write!(f,"{:?}", val)
            }
            Response::Text(val) => {
                write!(f,"{:?}", val)
            }
            Response::SciNumber(val) => write!(f, "{}", val),
            Response::Status(val) => write!(f, "{:?}", val),
            Response::EmissionCurrent(val) => write!(f, "{:?}", val),
            Response::PressureUnit(val) => write!(f, "{:?}", val),
        }
    }
}
#[cfg(not(feature="extra"))]
impl Display for Setting {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Setting::AddressOffset(address_offset) => write!(f, "SA{}",address_offset),
            Setting::TurnIonGaugeOn(turn_on) => write!(f,"IG{}", *turn_on as usize),
            Setting::TurnDegasOn(turn_on) => write!(f, "DG{}", *turn_on as usize),
            Setting::EmissionCurrent(emission_current) => write!(f, "SE{}", emission_current),
            Setting::Filament(filament) => write!(f, "SF{}", filament),
            Setting::SetOverPressurePoint(value) => write!(f, "SO{}",value),
            Setting::TripPointRelay1(pressure_point, value) => write!(f, "SL{}{}",pressure_point, value),
            Setting::SetFactoryDefaults => write!(f, "FAC"),
            Setting::BaudRate(baudrate) => write!(f, "SB{}",baudrate),
            Setting::Parity(parity) => write!(f, "SP{}", parity),
            Setting::ToggleUNLFunction => write!(f, "TLU"),
            Setting::UnlockCommsConfig => write!(f, "UNL"),
            Setting::Reset => write!(f, "RST"),
        }
    }
}
#[cfg(feature="extra")]
impl Display for Setting {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Setting::AddressOffset(address_offset) => write!(f, "SA{}",address_offset),
            Setting::TurnIonGaugeOn(turn_on) => write!(f,"IG{}", *turn_on as usize),
            Setting::TurnDegasOn(turn_on) => write!(f, "DG{}", *turn_on as usize),
            Setting::EmissionCurrent(emission_current) => write!(f, "SE{}", emission_current),
            Setting::Filament(filament) => write!(f, "SF{}", filament),
            Setting::SetOverPressurePoint(value) => write!(f, "SO{}",value),
            Setting::TripPointRelay1(pressure_point, value) => write!(f, "SL{}{}",pressure_point, value),
            Setting::SetFactoryDefaults => write!(f, "FAC"),
            Setting::BaudRate(baudrate) => write!(f, "SB{}",baudrate),
            Setting::Parity(parity) => write!(f, "SP{}", parity),
            Setting::ToggleUNLFunction => write!(f, "TLU"),
            Setting::UnlockCommsConfig => write!(f, "UNL"),
            Setting::Reset => write!(f, "RST"),
            Setting::PressureUnit(unit) => write!(f,"SU{}", unit),
        }
    }
}

#[cfg(not(feature="extra"))]
impl Display for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Value::IonGaugePressure => write!(f, "RD"),
            Value::IonGaugeIsOn => write!(f, "IGS"),
            Value::TripPointRelay1(pressure_point) => write!(f, "RL{}",pressure_point),
            Value::DegasStatusIsOn => write!(f, "DGS", ),
            Value::EmissionCurrentStatus => write!(f, "SES", ),
            Value::IonGaugeModuleStatus => write!(f, "RS", ),
            Value::SoftwareVersion => write!(f, "VER", ),
        }
    }
}
#[cfg(feature="extra")]
impl Display for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Value::IonGaugePressure => write!(f, "RD"),
            Value::IonGaugeIsOn => write!(f, "IGS"),
            Value::TripPointRelay1(pressure_point) => write!(f, "RL{}",pressure_point),
            Value::DegasStatusIsOn => write!(f, "DGS"),
            Value::EmissionCurrentStatus => write!(f, "SES" ),
            Value::IonGaugeModuleStatus => write!(f, "RS" ),
            Value::SoftwareVersion => write!(f, "VER"),
            Value::PressureUnit => write!(f,"RU"),
            Value::IonGaugeIonCurrent => write!(f, "RDIGC"),
            Value::IonGaugeEmissionCurrent => write!(f,"RDIGE"),
            Value::IonGaugeFilamentVoltage => write!(f, "RDIGV"),
            Value::IonGaugeFilamentCurrent => write!(f, "RDIGI"),
        }
    }
}