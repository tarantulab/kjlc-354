use embedded_hal::serial;

#[cfg(not(feature="extra"))]
use crate::cmd::{Message, Response, Setting, Status, Value, EmissionCurrent};

#[cfg(feature="extra")]
use crate::cmd::{Message, Response, Setting, Status, Value, PressureUnit, EmissionCurrent};

use crate::errors::{DriverError, Kjlc354Error, ResponseError};
use crate::scinumber::SciNumber;
use crate::*;
use core::fmt::Write;
use heapless::Vec;


/// Used to represent the status of the I/O operations in the device
#[derive(PartialEq, Debug)]
pub enum State {
    Idle,
    SendingMessage(Vec<u8, MAX_STR_LEN>),
    Flush,
    ReadFirst,
    ReadID(usize),
    ReadResult(Vec<u8, MAX_STR_LEN>),
    Failure,
}

/**
Representation of the device.

- `port`: Serial port used for communication. Must implement the traits from embedded_hal::serial

- `addr`: Configured RS-485 address in the device. By default, this should be 01 
  (device uses hex for address). Even in RS-232 operation mode it must be configured. 
  At driver startup, a message is automatically sent to assure the configured address 
  in the device is the specified one.

- `addrnum`: same as addr but as int to avoid formating to validate responses

- `state`: Tracks the current driver state (Reading, Writing, etc)
 
- `last_message`: Keeps metadata information of the last-sent message for troubleshooting and other tasks

- Timeout logic implementation is left for the user to implement.
*/
#[derive(Debug)]
pub struct Kjlc354<S>
where
    S: Serial,
{
    port: S,
    addr: [u8; ADDR_LEN],
    addrnum: usize,
    state: State,
    last_message: Message,
}
pub type NBResult<T, S> = nb::Result<
    T,
    DriverError<
        <S as serial::Read<u8>>::Error,
        <S as serial::Write<u8>>::Error,
    >,
>;

#[derive(Clone, Debug)]
/// Utility struct for storing query response data
pub struct ResponseData {
    query: Message,
    pub response: Option<Vec<u8, MAX_STR_LEN>>,
}

/// Wrapper function for embedded-hal serial read
fn read<S: Serial>(serial: &mut S) -> NBResult<u8, S> {
    serial
        .read()
        .map_err(|error| error.map(DriverError::SerialRead))
}

/// Wrapper function for embedded-hal serial write
fn write<S: Serial>(serial: &mut S, byte: u8) -> NBResult<(), S> {
    serial
        .write(byte)
        .map_err(|error| error.map(DriverError::SerialWrite))
}

/// Wrapper function for embedded-hal serial flush
fn flush<S: Serial>(serial: &mut S) -> NBResult<(), S> {
    serial
        .flush()
        .map_err(|error| error.map(DriverError::SerialWrite))
}

impl<S> Kjlc354<S>
where
    S: Serial,
{
    // Makes sure the ID of the device is in a valid range. If that is the case,
    // creates a new instance of the driver.
    pub fn new(port: S, address: u8) -> Kjlc354<S> {
        let mut addr = [0u8; ADDR_LEN];
        let mut buf = heapless::String::<2>::new();
        //will never fail
        write!(buf, "{:02X}", address).ok();
        let buf = buf.as_bytes();
        addr[0] = buf[0];
        addr[1] = buf[1];
        Kjlc354 {
            port,
            addr,
            addrnum: address as usize,
            last_message: Message::None,
            state: State::Idle,
        }
    }

    /// Returns the underlying serial port.
    pub fn take_port(self) -> S {
        self.port
    }

    /// Provide a configuration function to be applied to the underlying serial port.
    pub fn reconfig_port<F>(&mut self, config: F)
    where
        F: Fn(&mut S),
    {
        config(&mut self.port);
    }

    /// If a response is being processed, this will try to read as much info as possible from
    /// the serial bus in a non-blocking way. If the current state is not a response-reading one,
    /// it will return a wrapped `None` result.  The received response is not parsed inmediatly in
    /// case the information about the sent message is relevant for the user. If you just want the
    /// parsed data, the `parse_response` utility function is provided.
    pub fn poll(&mut self) -> NBResult<Option<ResponseData>, S> {
        if Message::Setting(Setting::Reset) == self.last_message {
            self.state = State::Idle;
            return Ok(Some(ResponseData {
                query: self.last_message.clone(),
                response: Some(Vec::new()),
            }));
        }
        let response: Option<ResponseData> = loop {
            match &mut self.state {
                State::ReadFirst => {
                    let ch = read(&mut self.port)?;
                    if ch == b'*' || ch == b'?' {
                        self.state = State::ReadID(0);
                    } else {
                        self.state = State::Failure;
                    }
                }
                //Read first digits of ID
                State::ReadID(i) => {
                    if read(&mut self.port)? == self.addr[*i] {
                        self.state = if *i < ADDR_LEN - 1 {
                            State::ReadID(*i + 1)
                        } else {
                            State::ReadResult(Vec::new())
                        };
                    } else {
                        self.state = State::Failure;
                    }
                }
                //Read the message content
                State::ReadResult(buffer) => {
                    let byte = read(&mut self.port)?;
                    match byte {
                        b'\r' => {
                            break Some(ResponseData {
                                query: self.last_message.clone(),
                                response: Some(core::mem::take(buffer)),
                            })
                        }
                        ch => {
                            if buffer.push(ch).is_err() {
                                self.state = State::Failure
                            };
                        }
                    }
                }
                State::Failure => {
                    while read(&mut self.port).is_ok() {}
                    self.state = State::Idle;
                    break Some(ResponseData {
                        query: self.last_message.clone(),
                        response: None,
                    });
                }
                State::Idle | State::SendingMessage(_) | State::Flush => {
                    let _ = read(&mut self.port)?;
                    break None;
                }
            }
        };
        self.state = State::Idle;
        Ok(response)
    }

    /// Produces and schedules the query text to be sent to the instrument.
    pub fn schedule_message(&mut self, msg: Message) -> NBResult<(), S> {
        match self.state {
            State::Idle => {
                let mut buffer = Vec::<u8, MAX_STR_LEN>::new();
                self.last_message = msg;
                write!(
                    &mut buffer,
                    "#{:02X}{}\r",
                    self.addrnum, self.last_message
                )
                .map_err(|_| DriverError::BadMessage)?;
                buffer.reverse();
                self.state = State::SendingMessage(buffer);
                Ok(())
            }
            _ => Err(nb::Error::WouldBlock),
        }
    }

    /// Calls `schedule_message` with the last message sent as the argument.
    /// Intended for error correction or repeated queries.
    pub fn reschedule_last_message(&mut self) -> NBResult<(), S> {
        self.schedule_message(self.last_message.clone())
    }

    /// Sends as much of the scheduled message in a non-blocking way.
    pub fn send_message(&mut self) -> NBResult<(), S> {
        loop {
            match &mut self.state {
                State::SendingMessage(buffer) => {
                    if let Some(byte) = buffer.last() {
                        write(&mut self.port, *byte)?;
                        buffer.pop();
                    } else {
                        self.state = State::Flush;
                    }
                }
                State::Flush => {
                    flush(&mut self.port)?;
                    self.state = State::ReadFirst
                }
                _ => break,
            }
        }
        Ok(())
    }
}

#[cfg(feature = "extra")]
/// Parser for strings following the KJLC 354 response message format.
/// It uses `data.query` to infer the datatype to be used in the parsing of the
/// contents.
pub fn parse_response(data: ResponseData) -> Result<Response, ResponseError> {

    if let Some(response) = data.response {
        let text = response.as_slice();
        match text {
            b" SYNTX ER" => {
                Err(ResponseError::InstrumentError(Kjlc354Error::SyntaxError))
            }
            b" COMM ERR" => Err(ResponseError::InstrumentError(
                Kjlc354Error::CommunicationsError,
            )),
            _ => match &data.query {
                Message::Value(value) => match value {
                    Value::IonGaugePressure
                    | Value::TripPointRelay1(_)
                    | Value::IonGaugeIonCurrent
                    | Value::IonGaugeEmissionCurrent
                    | Value::IonGaugeFilamentVoltage
                    | Value::IonGaugeFilamentCurrent => {
                        Ok(Response::SciNumber(
                            SciNumber::from_ascii(&text[1..]).map_err(
                                |_| ResponseError::FailedToParseResponse,
                            )?,
                        ))
                    }
                    Value::IonGaugeIsOn => match text {
                        b" 0 IG OFF" => Ok(Response::Boolean(false)),
                        b" 1 IG ON " => Ok(Response::Boolean(true)),
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                    Value::DegasStatusIsOn => match text {
                        b" 0 DG OFF" => Ok(Response::Boolean(false)),
                        b" 1 DG ON " => Ok(Response::Boolean(true)),
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                    Value::EmissionCurrentStatus => match text {
                        b" 0.1MA EM" => Ok(Response::EmissionCurrent(
                            EmissionCurrent::I100uA,
                        )),
                        b" 4.0MA EM" => Ok(Response::EmissionCurrent(
                            EmissionCurrent::I40mA,
                        )),
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                    Value::IonGaugeModuleStatus => match text {
                        b" 00 ST OK" => Ok(Response::Status(Status::Ok)),
                        b" 01 OVPRS" => {
                            Ok(Response::Status(Status::Overpressure))
                        }
                        b" 02 EMISS" => {
                            Ok(Response::Status(Status::EmissionCurrentFailure))
                        }
                        b" 08 POWER" => {
                            Ok(Response::Status(Status::PowerCycle))
                        }
                        b" 0A EMISS" => Ok(Response::Status(
                            Status::EmissionCurrentFailureAfterPowerCycle,
                        )),
                        b" 20 ION C" => {
                            Ok(Response::Status(Status::IonCurrentFailure))
                        }
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                    Value::SoftwareVersion => Ok(Response::Text(response)),
                    Value::PressureUnit => match text {
                        b" TORR    " => {
                            Ok(Response::PressureUnit(PressureUnit::Torricelli))
                        }
                        b" MBAR    " => {
                            Ok(Response::PressureUnit(PressureUnit::MiliBar))
                        }
                        b" PASCAL  " => {
                            Ok(Response::PressureUnit(PressureUnit::Pascal))
                        }
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                },
                Message::Setting(setting) => match &setting {
                    Setting::AddressOffset(_)
                    | Setting::TurnIonGaugeOn(_)
                    | Setting::TurnDegasOn(_)
                    | Setting::EmissionCurrent(_)
                    | Setting::Filament(_)
                    | Setting::SetOverPressurePoint(_)
                    | Setting::TripPointRelay1(_, _)
                    | Setting::SetFactoryDefaults
                    | Setting::BaudRate(_)
                    | Setting::Parity(_)
                    | Setting::UnlockCommsConfig
                    | Setting::PressureUnit(_) => {
                        if text == b" PROGM OK" {
                            Ok(Response::Boolean(true))
                        } else {
                            Err(ResponseError::FailedToParseResponse)
                        }
                    }
                    Setting::ToggleUNLFunction => match text {
                        b" 1 UL ON " => Ok(Response::Boolean(true)),
                        b" 0 UL OFF" => Ok(Response::Boolean(true)),
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                    Setting::Reset => Ok(Response::Boolean(true)),
                },
                Message::None => Err(ResponseError::IllegalState),
            },
        }
    } else {
        Err(ResponseError::FailedToParseResponse)
    }
}

#[cfg(not(feature = "extra"))]
/// Parser for strings following the KJLC 354 response message format.
/// It uses `data.query` to infer the datatype to be used in the parsing of the
/// contents.
pub fn parse_response(data: ResponseData) -> Result<Response, ResponseError> {

    if let Some(response) = data.response {
        let text = response.as_slice();
        match text {
            b" SYNTX ER" => {
                Err(ResponseError::InstrumentError(Kjlc354Error::SyntaxError))
            }
            b" COMM ERR" => Err(ResponseError::InstrumentError(
                Kjlc354Error::CommunicationsError,
            )),
            _ => match &data.query {
                Message::Value(value) => match value {
                    Value::IonGaugePressure
                    | Value::TripPointRelay1(_) => {
                        Ok(Response::SciNumber(
                            SciNumber::from_ascii(&text[1..]).map_err(
                                |_| ResponseError::FailedToParseResponse,
                            )?,
                        ))
                    }
                    Value::IonGaugeIsOn => match text {
                        b" 0 IG OFF" => Ok(Response::Boolean(false)),
                        b" 1 IG ON " => Ok(Response::Boolean(true)),
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                    Value::DegasStatusIsOn => match text {
                        b" 0 DG OFF" => Ok(Response::Boolean(false)),
                        b" 1 DG ON " => Ok(Response::Boolean(true)),
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                    Value::EmissionCurrentStatus => match text {
                        b" 0.1MA EM" => Ok(Response::EmissionCurrent(
                            EmissionCurrent::I100uA,
                        )),
                        b" 4.0MA EM" => Ok(Response::EmissionCurrent(
                            EmissionCurrent::I40mA,
                        )),
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                    Value::IonGaugeModuleStatus => match text {
                        b" 00 ST OK" => Ok(Response::Status(Status::Ok)),
                        b" 01 OVPRS" => {
                            Ok(Response::Status(Status::Overpressure))
                        }
                        b" 02 EMISS" => {
                            Ok(Response::Status(Status::EmissionCurrentFailure))
                        }
                        b" 08 POWER" => {
                            Ok(Response::Status(Status::PowerCycle))
                        }
                        b" 0A EMISS" => Ok(Response::Status(
                            Status::EmissionCurrentFailureAfterPowerCycle,
                        )),
                        b" 20 ION C" => {
                            Ok(Response::Status(Status::IonCurrentFailure))
                        }
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                    Value::SoftwareVersion => Ok(Response::Text(response)),
                },
                Message::Setting(setting) => match &setting {
                    Setting::AddressOffset(_)
                    | Setting::TurnIonGaugeOn(_)
                    | Setting::TurnDegasOn(_)
                    | Setting::EmissionCurrent(_)
                    | Setting::Filament(_)
                    | Setting::SetOverPressurePoint(_)
                    | Setting::TripPointRelay1(_, _)
                    | Setting::SetFactoryDefaults
                    | Setting::BaudRate(_)
                    | Setting::Parity(_)
                    | Setting::UnlockCommsConfig => {
                        if text == b" PROGM OK" {
                            Ok(Response::Boolean(true))
                        } else {
                            Err(ResponseError::FailedToParseResponse)
                        }
                    }
                    Setting::ToggleUNLFunction => match text {
                        b" 1 UL ON " => Ok(Response::Boolean(true)),
                        b" 0 UL OFF" => Ok(Response::Boolean(true)),
                        _ => Err(ResponseError::FailedToParseResponse),
                    },
                    Setting::Reset => Ok(Response::Boolean(true)),
                },
                Message::None => Err(ResponseError::IllegalState),
            },
        }
    } else {
        Err(ResponseError::FailedToParseResponse)
    }
}
#[cfg(test)]
mod tests {
    use crate::scinumber::Sign;

    use super::*;
    #[test]
    fn parse_scinumber() {
        let a = ResponseData {
            query: Message::Value(Value::IonGaugePressure),
            response: Some(heapless::Vec::from_slice(b" 7.90E-02").unwrap()),
        };
        let res = SciNumber::try_new(7, 90, Sign::Minus, 2).unwrap();
        assert_eq!(Ok(Response::SciNumber(res)), parse_response(a))
    }
}
