#![no_std]
#![no_main]

use bsp::{entry, hal};
use core::cell::RefCell;
use core::fmt::{self, Write};
use core::str::from_utf8;
use cortex_m::{interrupt::Mutex, peripheral::NVIC};
use hal::{
    clock::GenericClockController,
    delay::Delay,
    ehal::blocking::delay::DelayMs,
    ehal::digital::v2::ToggleableOutputPin,
    gpio::v2::{Pin, PushPullOutput, PA20, PA22},
    pac::{interrupt, CorePeripherals, Peripherals},
    time::Hertz,
    usb::{
        usb_device::{bus::UsbBusAllocator, prelude::*},
        UsbBus,
    },
};
use itsybitsy_m4 as bsp;
use kjlc_354::cmd::{PressurePoint, Response};
use kjlc_354::{
    cmd::{Message, Value},
    fsm::{self, Kjlc354, ResponseData},
};
use max485::{self, Max485};
#[cfg(not(feature = "use_semihosting"))]
use panic_halt as _;
#[cfg(feature = "use_semihosting")]
use panic_semihosting as _;
use usbd_serial::{SerialPort, USB_CLASS_CDC};

static MAILBOX: Mutex<RefCell<Option<ResponseData>>> =
    Mutex::new(RefCell::new(None));

type Rs485 = Max485<bsp::Uart, Pin<PA20, PushPullOutput>>;
static KJLC354: Mutex<RefCell<Option<Kjlc354<Rs485>>>> =
    Mutex::new(RefCell::new(None));

static mut LED: Option<Pin<PA22, PushPullOutput>> = None;
static mut USB_SERIAL: Option<USBSerial> = None;
static mut USB_BUS_ALLOCATOR: Option<UsbBusAllocator<UsbBus>> = None;

#[entry]
fn main() -> ! {
    let mut dp = Peripherals::take().unwrap();
    let mut core = CorePeripherals::take().unwrap();
    let mut clocks = GenericClockController::with_internal_32kosc(
        dp.GCLK,
        &mut dp.MCLK,
        &mut dp.OSC32KCTRL,
        &mut dp.OSCCTRL,
        &mut dp.NVMCTRL,
    );

    let pins = bsp::Pins::new(dp.PORT);
    let mut delay = Delay::new(core.SYST, &mut clocks);
    let bus_allocator = unsafe {
        USB_BUS_ALLOCATOR = Some(bsp::usb_allocator(
            dp.USB,
            &mut clocks,
            &mut dp.MCLK,
            pins.usb_dm,
            pins.usb_dp,
        ));
        USB_BUS_ALLOCATOR.as_ref().unwrap()
    };

    unsafe {
        USB_SERIAL = Some(USBSerial::new(bus_allocator));
    }

    let mut serialx = bsp::uart(
        &mut clocks,
        Hertz(9600),
        dp.SERCOM3,
        &mut dp.MCLK,
        pins.d0_rx,
        pins.d1_tx,
    );

    let mut index = 1;
    let mut raw_response: Option<ResponseData> = None;
    let sequence: [Message; 4] = [
        Value::SoftwareVersion.into(),
        Value::IonGaugeIsOn.into(),
        Value::IonGaugeModuleStatus.into(),
        Value::TripPointRelay1(PressurePoint::TurnsOnAbove).into(),
    ];
    cortex_m::interrupt::free(|cs| {
        unsafe {
            LED = Some(pins.d13.into_push_pull_output());
        }
        serialx.enable_interrupts(hal::sercom::v2::uart::Flags::RXC);
        let port =
            max485::Max485::new(serialx, pins.d10.into_push_pull_output());
        let mut kjlc354 = fsm::Kjlc354::new(port, 1);

        let _ = nb::block!(kjlc354.schedule_message(sequence[0].clone()));
        let _ = nb::block!(kjlc354.send_message());

        KJLC354.borrow(cs).replace(Some(kjlc354));
    });
    unsafe {
        core.NVIC.set_priority(interrupt::USB_OTHER, 1);
        core.NVIC.set_priority(interrupt::USB_TRCPT0, 1);
        core.NVIC.set_priority(interrupt::USB_TRCPT1, 1);
        core.NVIC.set_priority(interrupt::SERCOM3_2, 1);

        NVIC::unmask(interrupt::USB_OTHER);
        NVIC::unmask(interrupt::USB_TRCPT0);
        NVIC::unmask(interrupt::USB_TRCPT1);
        NVIC::unmask(interrupt::SERCOM3_2);
    }

    loop {
        delay.delay_ms(400u16);
        cortex_m::interrupt::free(|cs| {
            if let Some(raw) = MAILBOX.borrow(cs).replace(None) {
                raw_response = Some(raw);
            }
        });
        if let Some(raw) = raw_response {
            match fsm::parse_response(raw) {
                Ok(resp) => {
                    unsafe {
                        if let Response::Text(text) = resp {
                            if let Some(usb) = USB_SERIAL.as_mut() {
                                let _ = write!(
                                    usb,
                                    "{:?}\r\n",
                                    from_utf8(text.as_slice())
                                );
                            }
                        } else if let Some(usb) = USB_SERIAL.as_mut() {
                            let _ = write!(usb, "{:?}\r\n", resp);
                        }
                        raw_response = None;
                    }
                    index = if index == 3 { 0 } else { index + 1 };
                    cortex_m::interrupt::free(|cs| {
                        if let Some(kjlc354) =
                            KJLC354.borrow(cs).borrow_mut().as_mut()
                        {
                            let _ = nb::block!(kjlc354
                                .schedule_message(sequence[index].clone()));
                            let _ = nb::block!(kjlc354.send_message());
                        }
                    });
                }
                Err(err) => {
                    unsafe {
                        if let Some(usb) = USB_SERIAL.as_mut() {
                            let _ = write!(
                                usb,
                                "Failure caused by: {:?} \r\n",
                                err
                            );
                        }
                        raw_response = None;
                    }
                    cortex_m::interrupt::free(|cs| {
                        if let Some(kjlc354) =
                            KJLC354.borrow(cs).borrow_mut().as_mut()
                        {
                            //Communication failed for x o y reason
                            let _ =
                                nb::block!(kjlc354.reschedule_last_message());
                            let _ = nb::block!(kjlc354.send_message());
                        }
                    });
                }
            }
        }
    }
}

fn poll_usb() {
    unsafe {
        if let Some(usb) = USB_SERIAL.as_mut() {
            usb.poll();
            usb.flush();

            let mut buf = [0u8; 64];
            if let Ok(count) = usb.read(&mut buf) {
                for (i, c) in buf.iter().enumerate() {
                    if i >= count {
                        break;
                    }
                    let _ = write!(usb, "{}", c);
                }
            }
            toggle();
        }
    }
}

#[interrupt]
fn USB_OTHER() {
    poll_usb();
}

#[interrupt]
fn USB_TRCPT0() {
    poll_usb();
}

#[interrupt]
fn USB_TRCPT1() {
    poll_usb();
}

#[interrupt]
fn SERCOM3_2() {
    cortex_m::interrupt::free(|cs| {
        if let Some(kjlc354) = KJLC354.borrow(cs).borrow_mut().as_mut() {
            if let Ok(Some(response)) = kjlc354.poll() {
                MAILBOX.borrow(cs).replace(Some(response));
            }
        } else {
            toggle();
            cortex_m::interrupt::disable();
            panic!("interrupt can't be cleared");
        }
    })
}

fn toggle() {
    unsafe {
        if let Some(x) = LED.as_mut() {
            x.toggle().unwrap();
        }
    }
}

pub struct USBSerial<'a> {
    serial: SerialPort<'a, UsbBus>,
    bus: UsbDevice<'a, UsbBus>,
}
impl<'a> USBSerial<'a> {
    pub fn new(bus_allocator: &'a UsbBusAllocator<UsbBus>) -> Self {
        USBSerial {
            serial: SerialPort::new(bus_allocator),
            bus: UsbDeviceBuilder::new(
                bus_allocator,
                UsbVidPid(0x16c0, 0x27dd),
            )
            .manufacturer("Fake company")
            .product("Serial port")
            .serial_number("TEST")
            .device_class(USB_CLASS_CDC)
            .build(),
        }
    }
    pub fn poll(&mut self) {
        self.bus.poll(&mut [&mut self.serial]);
    }
    pub fn flush(&mut self) {
        let _ = self.serial.flush();
    }
    pub fn read(&mut self, buf: &mut [u8]) -> Result<usize, UsbError> {
        self.serial.read(buf)
    }
}

impl fmt::Write for USBSerial<'_> {
    fn write_str(&mut self, string: &str) -> core::fmt::Result {
        let mut string = string.as_bytes();

        while !string.is_empty() {
            match self.serial.write(string) {
                Ok(count) => string = &string[count..],
                Err(UsbError::WouldBlock) => continue,
                Err(_) => return Err(fmt::Error),
            }
        }

        Ok(())
    }
}
